let gridon1;
let canvas = new fabric.Canvas('c1');

canvas.setWidth($('#canvas-wrapper').width());
canvas.setHeight($('#canvas-wrapper').height());
$(window).on('resize', function(){
    canvas.setWidth($('#canvas-wrapper').width());
    canvas.setHeight($('#canvas-wrapper').height());
});

let count;
let canvasWidth=canvas.getWidth();
let canvasHeight=canvas.getHeight();

canvas.custom_attribute_array = ["price","type", "availability", "code", "fill", "imageID", "id","positionId"];
let ready = false;
let code = JSON.stringify(canvas.toDatalessJSON());
let image = canvas.toSVG(['id','code','price','availability','positionId']);
if(document.getElementById("skip"))
    document.getElementById("skip").style.display = "none";
if(document.getElementById("upload360"))
document.getElementById("upload360").style.display = "none";
if(document.getElementById("finish"))
document.getElementById("finish").style.display = "none";
if(document.getElementById("previous"))
document.getElementById("previous").style.display = "none";
if(document.getElementById("edit360"))
    document.getElementById("edit360").style.display = "none";
if(document.getElementById("setSeatNo"))
document.getElementById("setSeatNo").style.display = "none";
if(document.getElementById("skip1"))
    document.getElementById("skip1").style.display = "none";


if (~window.location.href.indexOf("create?")) {

    window.history.pushState('', '', '/create');
}

//set arrow keys to move objcts
let processKeys = function (evt) {
    evt = evt || window.event;

    let movementDelta = 5;

    let activeObject = canvas.getActiveObject();
    let activeGroup = canvas.getActiveObjects();

    if (evt.keyCode === 37) {
        evt.preventDefault(); // Prevent the default action
        if (activeObject) {
            let a = activeObject.get('left') - movementDelta;
            activeObject.set('left', a);

        }
        else if (activeGroup) {
            let a = activeGroup.get('left') - movementDelta;
            activeGroup.set('left', a);
        }

    } else if (evt.keyCode === 39) {
        evt.preventDefault(); // Prevent the default action
        if (activeObject) {
            let a = activeObject.get('left') + movementDelta;
            activeObject.set('left', a);
        }
        else if (activeGroup) {
            let a = activeGroup.get('left') + movementDelta;
            activeGroup.set('left', a);
        }

    } else if (evt.keyCode === 38) {
        evt.preventDefault(); // Prevent the default action
        if (activeObject) {
            let a = activeObject.get('top') - movementDelta;
            activeObject.set('top', a);
        }
        else if (activeGroup) {
            let a = activeGroup.get('top') - movementDelta;
            activeGroup.set('top', a);
        }

    } else if (evt.keyCode === 40) {
        evt.preventDefault(); // Prevent the default action
        if (activeObject) {
            let a = activeObject.get('top') + movementDelta;
            activeObject.set('top', a);
        }
        else if (activeGroup) {
            let a = activeGroup.get('top') + movementDelta;
            activeGroup.set('top', a);
        }
    }

    if (activeObject) {
        activeObject.setCoords();
        canvas.renderAll();
    } else if (activeGroup) {
        activeGroup.setCoords();
        canvas.renderAll();
    }

    if (evt.keyCode === 46) {
        seldel(); // Prevent the default action
    }
};


let rectangle=[];
let length=rectangle.length;
let id=[];


//grid system apply********************************************************************************


function turnOnGrid(){
    if(gridon1==0){
        canvas.selection = true;
        let grid = 25;

// create grid

        for (let i = 0; i < (canvasWidth / grid); i++) {
            let line=new fabric.Line([ i * grid, 0, i * grid, canvasHeight], { stroke: '#ccc', selectable: false });
            canvas.add(line);
            canvas.sendToBack(line);
            let line2=new fabric.Line([ 0, i * grid, canvasWidth, i * grid], { stroke: '#ccc', selectable: false });
            canvas.add(line2);
            canvas.sendToBack(line2);
        }

// snap to grid

        canvas.on('object:moving', function(options) {
            options.target.set({
                left: Math.round(options.target.left / grid) * grid,
                top: Math.round(options.target.top / grid) * grid
            });
        });
    }

}

canvas.selection = true;
canvas.allowTouchScrolling=true;
canvas.imageSmoothingEnabled=true;

//create rectangle
//add to array
//and add to canvas

let createRect= $("#createRectangle");
if(createRect && createRect.length>0) {
    createRect.click(function () {

        id.push(0);
        rectangle.push(new fabric.Rect({
            width: 40,
            height: 40,
            left: 10,
            top: 10,
            fill: '#805300',
            // fill: 'rgba(0,0,0,0)',
            strokeWidth: 0.3,
            stroke: 'black',
            type: 'rect',
            availability: "available",
            positionId:"",
            code: "",
            imageID: ''

        }));


        let l = rectangle.length;
        canvas.add(rectangle[l - 1]);
    });

}

let createBox= $("#box");
if(createBox && createBox.length>0) {
    createBox.click(function () {

        id.push(0);
        rectangle.push(new fabric.Rect({
            width: 80,
            height: 80,
            left: 100,
            top: 100,
            fill: 'rgba(0,0,0,0)',
            strokeWidth: 3,
            stroke: 'black',
            type: 'rect',
            availability: "available",
            positionId:"",
            code: "",
            imageID: ''

        }));
        let l = rectangle.length;
        canvas.add(rectangle[l - 1]);
    });

}



//create one seat
let oneSeat = $("#addOneSeat");
if(oneSeat && oneSeat.length>0) {
    oneSeat.click(function () {

        id.push(1);
        rectangle.push(new fabric.Rect({
            width: 25,
            height: 25,
            left: 60,
            top: 70,
            fill: '#e9967a',
            strokeWidth: 0.3,
            stroke: 'black',
            rx: 5,
            ry: 5,
            type: 'rect',
            availability: "available",
            positionId:"",
            code: "",
            price: '0.00',
            id:'0'
        }));

        let l = rectangle.length;
        canvas.add(rectangle[l - 1]);
    });

}


//create circle
//add to array
//and add to canvas
let createCircle = $("#addCircle");
if (createCircle && createCircle.length>0) {
    createCircle.click(function () {

        rectangle.push(new fabric.Circle({
            radius: 30,
            fill: '#ff7f50',
            top: 100,
            left: 100,
            strokeWidth: 0.3,
            stroke: 'black',
            opacity: '1',
        }));

        canvas.selectionColor = 'rgba(0,255,0,0.3)';
        canvas.selectionBorderColor = 'red';
        canvas.selectionLineWidth = 1;
        let i = rectangle.length;
        canvas.add(rectangle[i - 1]);
    });
}

let CircleStroke = $("#circleStroke");
if (CircleStroke && CircleStroke.length>0) {
    CircleStroke.click(function () {

        rectangle.push(new fabric.Circle({
            radius: 40,
            fill: 'rgba(0,0,0,0)',
            top: 160,
            left: 160,
            strokeWidth: 3,
            stroke: 'black',
            opacity: '1',
        }));

        canvas.selectionColor = 'rgba(0,255,0,0.3)';
        canvas.selectionBorderColor = 'red';
        canvas.selectionLineWidth = 1;
        let i = rectangle.length;
        canvas.add(rectangle[i - 1]);
    });
}

let createTriangle = $("#addTriangle");
if (createTriangle && createTriangle.length>0) {
    createTriangle.click(function () {

        rectangle.push(new fabric.Triangle({
            width: 100,
            height: 100,
            left: 50,
            top: 300,
            fill: 'red',
            strokeWidth: 0.3,
            stroke: 'black',
        }));
        let o = rectangle.length;
        canvas.add(rectangle[o - 1]);
    });
}


let triangleStroke = $("#TriangleStroke");
if (triangleStroke && triangleStroke.length>0) {
    triangleStroke.click(function () {

        rectangle.push(new fabric.Triangle({
            width: 80,
            height: 80,
            left: 50,
            top: 200,
            fill: 'rgb(0,0,0,0)',
            strokeWidth: 3,
            stroke: 'black',
        }));
        let o = rectangle.length;
        canvas.add(rectangle[o - 1]);
    });
}

//Add text to canvas
let addText = $("#textAdd");
if (addText && addText.length>0) {
    addText.click(function () {

        let fontsize;
        fontsize = $("#fnt").val();
        let txt;
        txt = document.getElementById("txt").value;
        if (txt == "") {
            alert("Enter some text");

        }
        else {
            rectangle.push(new fabric.IText(txt, {
                left: 60,
                top: 150,
                fill: 'Black',
                fontSize: fontsize
            }));
            let o = rectangle.length;
            canvas.add(rectangle[o - 1]);
        }
    });
}

//get width and hight of object
canvas.on('object:scaling', onObjectScaled);

function onObjectScaled(e) {
    let scaledObject = e.target;

    document.getElementById("demo").innerHTML = "Height = " + Math.round(scaledObject.getScaledHeight()) + " " + "Width =  " + Math.round(scaledObject.getScaledWidth());

}

let rowNumber;
let columnNumber;
let prices=0.00;

//set raws and cols for seat pack
let seatAdder = $('#addSeats');
if(seatAdder && seatAdder.length > 0) {
    seatAdder.click(function () {

        try {
            if (36 > $("#ex4").val()) {
                rowNumber = $("#ex4").val();
            }
            else {
                alert("Please Insert no of rows below 11");
            }
            if (16 > $("#ex3").val()) {
                columnNumber = $("#ex3").val();
            }
            else {
                alert("Please Insert no of cols below 11");
            }
            if ("" != $("#ex5").val()) {
                prices = $("#ex5").val();
            }
            else {
                alert("Default price 0.00 are taged");
            }
            seatPack();

        } catch (err) {
        }
    });
}

//create round rectangles pack

function seatPack() {
    for (let f = 1; f <= rowNumber; f++) {
        for (let p = 1; p <= columnNumber; p++) {


            rectangle.push(new fabric.Rect({
                width: 25,
                height: 25,
                left: 10 + 26 * f,
                top: 10 + 26 * p,
                fill: '#e9967a',
                strokeWidth: 0.3,
                stroke: 'black',
                rx: 5,
                ry: 5,
                type: 'rect',
                availability: "available",
                positionId:"",
                code: "",
                imageID: '',
                price: '0.00',
                id: '0'
            }));

            let l = rectangle.length;
            canvas.add(rectangle[l - 1]);
        }
    }
}


//line drawing
let LineEnable;

function drawLine() {
    let line2, isDown;
    canvas.on('mouse:down', function (o) {
        if (LineEnable == 0) {
            isDown = true;
            let pointer = canvas.getPointer(o.e);
            let points = [pointer.x, pointer.y, pointer.x, pointer.y];
            line2 = new fabric.Line(points, {
                strokeWidth: 5,
                fill: 'red',
                stroke: 'black',
                originX: 'center',
                originY: 'center',
                type: 'line'
            });
            canvas.add(line2);
        }
    });
    canvas.on('mouse:move', function (o) {
        if (!isDown) return;
        let pointer = canvas.getPointer(o.e);
        line2.set({x2: pointer.x, y2: pointer.y});
        canvas.renderAll();
    });
    canvas.on('mouse:dblclick', function (o) {
        isDown = false;
        LineEnable = 1;
        canvas.renderAll();

    });


}


//disable active line drawing function
let LineDisable = $("#drawLineDisable");
if (LineDisable && LineDisable.length>0) {
    LineDisable.click(function () {

        LineEnable = 1;
        $("#enable").show();
        $("#disable").hide();

    });
}

//call line drawing function
let lineDrawingStart = $("#enable");
if (lineDrawingStart && lineDrawingStart.length>0) {
    lineDrawingStart.click(function () {

        LineEnable = 0;
        drawLine();
    });
}


//get xand y coodinates of selected object
canvas.on('mouse:down', function () {
    let activeObject = canvas.getActiveObject();
    if (activeObject) {
        let x = Math.round(activeObject.left);
        let y = Math.round(activeObject.top);
        try {
            $('#objData-image').show();
            $('#objData-type').html('<b>Type: </b>' + activeObject.type);
            $('#objData-price').html('<b>Price: </b>' + activeObject.price);
            $('#objData-color').html('<b>Color: </b>' + activeObject.fill);
            $('#objData-availability').html('<b>Availability: </b>' + activeObject.availability);
            $('#objData-id').html('<b>ID: </b>' + activeObject.id);
            $('#objData-left').html('<b>Left: </b>' + x);
            $('#objData-top').html('<b>Top: </b>' + y);
            $('#objData-code').html('<b>Code: </b>' + activeObject.code);
            if(activeObject.type == 'rect'){
                $('#objData-photo').html('<b>Image ID: </b>' + activeObject.imageID);
            }


        } catch (err) {

        }
    }else {
        $('#objData-image').hide();

    }
});


//get x and y coodinates of mouse pont
let xx;
let yy;
canvas.on('mouse:move', function (options) {
    let canvasTemp = document.getElementById("c1");

    if (options.e.pageX || options.e.pageY) {
        xx = options.e.pageX;
        yy = options.e.pageY;
    }
    else {
        xx = options.e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        yy = options.e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    xx -= canvasTemp.offsetLeft;
    yy -= canvasTemp.offsetTop;

try {
    count=canvas.getObjects().length;
    $('#objCount').html(count);
}
   catch (err) {

   }

    document.getElementById("nemo").innerHTML = "x = " + (xx-150) + "  " + "y =  " + (yy-590);


});


//add itext


let tx;

function addText() {


    canvas.on('mouse:down', function (options) {
        if (tx == 0) {

            let left1 = xx - 139;
            let top1 = yy - 343;


            let newText = new fabric.IText("", {
                left: left1,
                top: top1,
                fontSize: 23,
                fontStyle: 'italic'
            });

            canvas.add(newText);

            canvas.setActiveObject(newText);
            newText.enterEditing();
            newText.selectAll();
        }
    });


}


//itext on
function on() {
    tx = 0;
    addText();
    $("#ton").hide();
    $("#toff").show();
}

//itext off
function offf() {
    tx = 1;
    $("#ton").show();
    $("#toff").hide();
}

//grid on
let gridOn = $("#gridOn");
if (gridOn && gridOn.length > 0) { //what happens in this line?
    gridOn.click(function () {
        gridon1 = 0;
        turnOnGrid();
        $("#gridOn").hide();
        $("#gridOff").show();
    });
}

//grid off
let turnOffGrid = $("#gridOff");
if (turnOffGrid && turnOffGrid.length > 0) {
    turnOffGrid.click(function () {

        gridon1 = 1;
        try {
            canvas.getObjects().forEach((obj) => {
                if (obj.type == "line") {
                    // alert(obj);
                    canvas.remove(obj);
                    // canvas.renderAll();
                }
                canvas.renderAll();
                canvas.off('object:moving');
            });
            canvas.renderAll();


        } catch (err) {
        }


        $("#gridOn").show();
        $("#gridOff").hide();
    });
}

//Functions for features in the canvas ===========================================================================================
//separate>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
let deleteCanvas = $("#deleteObjects");
if(deleteCanvas && deleteCanvas.length>0) {
    deleteCanvas.click(function () {

        if (confirm("Are You Sure??") == true) {
            canvas.clear();
            rectangle = [];
            id = [];

        } else {

        }

    });
}


//delete selected object
let selectedDelete = $("#deleteSelected");
if(selectedDelete && selectedDelete.length>0) {
    selectedDelete.click(function () {

        try {
            let deleted=canvas.getActiveObjects();
            canvas.getActiveObjects().forEach((obj) => {
                canvas.remove(obj)
            });
            canvas.discardActiveObject().renderAll()
        }

        catch (err) {}


    });
}


//set opacity of the obj
let setOpacity = $("#opacitySet");
if(setOpacity && setOpacity.length>0) {
    setOpacity.click(function () {
        let ob;
        ob = document.getElementById("opa").value;

        if (ob == "") {
            alert("Set opacity value in Text box");
        }

        else {

            if ((parseInt(ob) > 1) || (parseInt(ob) < 0)) {
                alert("Set value between 0-1")
            }
            else {
                let objgroup = canvas.getActiveObjects();
                let obj = canvas.getActiveObject();
                if (obj) {

                    obj.set('opacity', ob );
                    canvas.renderAll();
                }
                if (objgroup) {
                    if (objgroup.length > 1) {
                        canvas.getActiveObjects().forEach((obj) => {
                            obj.set('opacity', ob );
                        });
                        canvas.renderAll();

                    }
                }
                if (!obj && !objgroup) {
                    alert("There is no selected object to set opacity")
                }
            }
        }
    });
}


//set opacity of the obj
let setStrokeSize = $("#strokeSet");
if(setStrokeSize && setStrokeSize.length>0) {
    setStrokeSize.click(function () {
        let valueObj;
        valueObj = document.getElementById("stroke").value;
let ob = parseFloat(valueObj);
        if (ob == "") {
            alert("Set stroke value in Text box");
        }

        else {

            if ((parseInt(ob) < 0) ) {
                alert("Value cannot be less than 0")
            }
            else {
                let objgroup = canvas.getActiveObjects();
                let obj = canvas.getActiveObject();
                if (obj) {

                    obj.set('strokeWidth', ob );
                    canvas.renderAll();
                }
                if (objgroup) {
                    if (objgroup.length > 1) {
                        canvas.getActiveObjects().forEach((obj) => {
                            obj.set('strokeWidth', ob );
                        });
                        canvas.renderAll();

                    }
                }
                if (!obj && !objgroup) {
                    alert("There is no selected object to set opacity")
                }
            }
        }
    });
}


//remove border of obj
let removeBorder = $("#borderRemove");
if(removeBorder && removeBorder.length>0) {
    removeBorder.click(function () {
        let objSelected=0;

        try {

            if (canvas.getActiveObjects().length>0) {
                canvas.getActiveObjects().forEach((obj) => {
                    obj.set('stroke', "");
                    obj.set('strokeWidth', 0);

                });
                canvas.renderAll();
                objSelected=canvas.getActiveObjects().length;

            }
            else if (objSelected==0) {

                alert("Object not selected");

            }

        } catch (err) {
        }
    });
}

//lock the item that selected
let lockItem = $("#itemLock");
if(lockItem && lockItem.length>0) {
    lockItem.click(function () {

        let objSelected=0;
        try {

            if (canvas.getActiveObjects().length>0) {
                canvas.getActiveObjects().forEach((obj) => {
                    obj.lockMovementX = true;
                    obj.lockMovementY = true;
                    obj.lockRotation = true;
                    obj.lockScalingX = true;
                    obj.lockScalingY = true;
                });
                canvas.renderAll();
                objSelected=canvas.getActiveObjects().length;

            }
            else if (objSelected==0) {

                alert("Select an object to lock");

            }

        } catch (err) {
        }
    });
}


//unlock the item that selected
let unlockItem = $("#itemUnlock");
if(unlockItem && unlockItem.length>0) {
    unlockItem.click(function () {

        let objSelected=0;
        try {

            if (canvas.getActiveObjects().length>0) {
                canvas.getActiveObjects().forEach((obj) => {
                    obj.lockMovementX = false;
                    obj.lockMovementY = false;
                    obj.lockRotation = false;
                    obj.lockScalingX = false;
                    obj.lockScalingY = false;

                });
                canvas.renderAll();
                objSelected=canvas.getActiveObjects().length;

            }
            else if (objSelected==0) {

                alert("Select an object to unlock");

            }

        } catch (err) {
        }
    });
}


let sendToBack = $("#sendBack");
if(sendToBack && sendToBack.length>0) {
    sendToBack.click(function () {

        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Please Select a object");
        }
        if (obj) {

            canvas.sendToBack(obj);
            canvas.renderAll();
            canvas.discardActiveObject();
        }
        canvas.renderAll();
    });
}


let bringToFront = $("#bringFront");
if(bringToFront && bringToFront.length>0) {
    bringToFront.click(function () {

        let obj=canvas.getActiveObject();
        if(!obj)
        {alert("Please Select a object");
        }
        if(obj){

            canvas.bringToFront(obj);
            canvas.renderAll();
            canvas.discardActiveObject();
        }
        canvas.renderAll();
    });
}


let sendBackwards = $("#sendBackwards");
if(sendBackwards && sendBackwards.length>0) {
    sendBackwards.click(function (intersecting) {

        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Please Select a object");
        }
        if (obj) {
            canvas.sendBackwards(obj, intersecting);
            canvas.renderAll();
            canvas.discardActiveObject();
        }
        canvas.renderAll();

    });
}


let bringForward = $("#bringForwards");
if(bringForward && bringForward.length>0) {
    bringForward.click(function (intersecting) {
        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Please Select a object");
        }
        if (obj) {
            canvas.bringForward(obj, intersecting);
            canvas.renderAll();
            canvas.discardActiveObject();
        }
        canvas.renderAll();


    });
}


//set font style
let fontStyle = $("#styleFont");
if(fontStyle && fontStyle.length>0) {
    fontStyle.click(function () {
        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Text not selected");
        } else {
            if (obj.type == "i-text") {
                obj.fontStyle="italic";
                canvas.renderAll();
            }
        }

    });
}

let underline = $("#underlineText");
if(underline && underline.length>0) {
    underline.click(function () {

        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Text not selected");
        } else {
            if (obj.type == "i-text") {
                obj.set('underline', true);
                console.log(obj.type);
                canvas.renderAll();
            }
        }

    });
}



let boldFont = $("#bold");
if(boldFont && boldFont.length>0) {
    boldFont.click(function () {
        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Text not selected");
        } else {
            if (obj.type == "i-text") {
                if (obj.fontStyle=="italic") {
                    obj.fontStyle="oblique";
                    canvas.renderAll();
                    console.log(obj);
                }
                else {
                    obj.fontStyle = "bold";
                    canvas.renderAll();
                }
            }
        }

    });
}

let regularFont = $("#regular");
if(regularFont && regularFont.length>0) {
    regularFont.click(function () {
        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Text not selected");
        } else {
            if (obj.type == "i-text") {

                    obj.fontStyle = "normal";
                    obj.set ('underline',false);
                    canvas.renderAll();

            }
        }

    });
}

const div = $('#code');
if($('#objData') && $('#objData').length > 0){
    let uploaded = false;
    let file = '';
    let fileName = '';
    const link = div.attr('data-link');
    const id = div.attr('data-id');
    const url = link + 'addimage';
    let form = $('#objData').find('form');
    let uploadBtn = $('#objData').find('button');
    let formData = new FormData(form[0]);
    $('#file-upload').change(function(e){
        uploaded = false;
        file = e.target.files[0];
        fileName = e. target. files[0].name;
        formData.append('File', file, fileName);
        formData.append('ID', id);
    });

    uploadBtn.show();
    uploadBtn.click(function () {
        if(file != '' && !uploaded){

            $.ajax({
                url: url,
                type: 'POST',
                cache: false,
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    let result = JSON.parse(response);
                    $('#file-upload').val('');
                    uploaded = true;
                    let activeObject = canvas.getActiveObject();
                    if(activeObject.type == 'rect'){
                        activeObject.set('imageID', result.ImageID);
                        $('#objData-photo').html('<b>Image ID: </b>' + activeObject.imageID);
                    }
                    canvas.renderAll();

                },
                complete: function () {

                },
                error: function () {
                    $('#file-upload').val('');
                    uploaded = true;
                    alert('error alert');
                },
            });
        }
    });


}

//Features for canvas end here========================================================================================
//separate>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



//color JS for canvas start here========================================================================================
//separate>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



//set the price of seat
let setPrice = $("#PricingID");    //function for deleting objects in button
if(setPrice && setPrice.length > 0) {
    setPrice.click(function () {
        try {
            // alert("got you");
            let obj = canvas.getActiveObject();
            let objgroup = canvas.getActiveObjects();
            let w = document.getElementById("priceID").value;
            if (obj && obj.type == "rect" && w>0) {
                obj.set("price", w);
                alert("Seat is Priced for Rs. "+ w );
                // console.log(obj);
            }
            else if (objgroup && objgroup.length > 0 && w>0) {

                objgroup.forEach((obj) => {
                    if (obj.type == "rect") {
                        obj.set("price", w);
                    }
                });
                alert("Seats are Priced for Rs. "+ w );

                console.log(objgroup);
            } else {

                if(objgroup && objgroup.length > 0 && w==0){
                    alert("Price should be larger than 0 ");
                }
                else {

                    alert("please select single seat or group of seats");
                }

            }
        } catch (err) {
        }
    });
}
let nextValue;
let link360;
let setID = $('#idSet');    //function for setting ID in button
if(setID && setID.length > 0) {
    setID.click(function () {

        let myId = 1; //id for elements
        let priceEmpty = 0;
        canvas.getObjects().forEach((obj) => {
            if (obj.type == "rect" && obj.rx >0 && obj.ry >0) {
                obj.set("id", myId);
                obj.set('code', 'seat'+myId);
                myId++;
                if (obj.price == "0.00") {
                    priceEmpty++;
                    return 0;
                }
            }
        });


        if (priceEmpty > 0) {
            alert("You have to set prices for " + priceEmpty + " more seats...");
            return 0;
        }
        $("#idSet").hide();
        nextValue=1;
        ready = true;
        save();
        try {

            canvas.discardActiveObject();
            let sel = new fabric.ActiveSelection(canvas.getObjects(), {
                canvas: canvas,
            });
            canvas.setActiveObject(sel);
            canvas.requestRenderAll();

            if (canvas.getActiveObjects().length>0) {
                canvas.getActiveObjects().forEach((obj) => {


                    if (obj.id>0) {
                        obj.selectable = true;
                        obj.lockMovementX = true;
                        obj.lockMovementY = true;
                        obj.lockRotation = true;
                        obj.lockScalingX = true;
                        obj.lockScalingY = true;
                    }
                    else{
                        obj.lockMovementX = false;
                        obj.lockMovementY = false;
                        obj.lockRotation = false;
                        obj.lockScalingX = false;
                        obj.lockScalingY = false;
                        obj.selectable = false;
                    }
                });

                canvas.renderAll();

            }

            canvas.discardActiveObject().renderAll()
        } catch (err) {
        }

        document.getElementById('skip').removeAttribute('disabled');
        document.getElementById('upload360').removeAttribute('disabled');
        document.getElementById('previous').removeAttribute('disabled');
        document.getElementById("previous").style.display = "block";
        document.getElementById("skip").style.display = "block";
        document.getElementById("upload360").style.display = "none";
        document.getElementById("tools1").style.display = "none";
        document.getElementById("tools2").style.display = "none";
        document.getElementById("tools5").style.display = "none";
        document.getElementById("setSeatNo").style.display = "block";
        console.log(nextValue);


         link360 = div.attr('data-sphere');
        console.log(link360);
        if (link360!=0) {
            document.getElementById("edit360").style.display = "block";
            console.log(nextValue);

        }else{
            document.getElementById("edit360").style.display = "none";
            console.log(nextValue);

        }

    });
}

let skip = $('#skip');
if(skip && skip.length > 0) {
    skip.click(function () {
        if(nextValue==1) {
            nextValue = 2;

            $("#upload360").hide();
            document.getElementById('finish').removeAttribute('disabled');
            document.getElementById('skip').removeAttribute('disabled');
            document.getElementById("finish").style.display = "none";
            document.getElementById("upload360").style.display = "block";
            document.getElementById("skip").style.display = "none";
            document.getElementById("skip1").style.display = "block";
            document.getElementById("edit360").style.display = "none";
            console.log(nextValue);
            if (link360!=0) {
                document.getElementById("edit360").style.display = "block";
                console.log(nextValue);

            }else{
                document.getElementById("edit360").style.display = "none";
                console.log(nextValue);

            }
        }

    });
}

let skip1 = $('#skip1');
if(skip1 && skip1.length > 0) {
    skip1.click(function () {

        if(nextValue==2) {
            nextValue=3;
            $("#upload360").hide();
            document.getElementById('finish').removeAttribute('disabled');
            document.getElementById('skip').removeAttribute('disabled');
            document.getElementById("finish").style.display = "block";
            document.getElementById("upload360").style.display = "none";
            document.getElementById("skip").style.display = "none";
            document.getElementById("skip1").style.display = "none";
            document.getElementById("edit360").style.display = "none";
            console.log(nextValue);

        }
    });
}

let edit360 = $('#edit360');
if(edit360 && edit360.length > 0) {
    edit360.click(function () {

        const link = div.attr('data-editsphere');
        window.location.replace(link);

    });
}


let upload = $('#upload360');
if(upload && upload.length > 0) {
    upload.click(function (e) {


            e.preventDefault();
            nextValue=2;
            let uploadfile = document.getElementById('360file');
            let file = '';
            let fileName = '';
            const link = div.attr('data-link');
            const id = div.attr('data-id');
            const url = link + 'addsphere';
            let form = $('#360uploadform');
            let formData = new FormData(form[0]);
            uploadfile.click();
            uploadfile.onchange = function (e) {
                file = e.target.files[0];
                fileName = e. target. files[0].name;
                formData.append('File', file, fileName);
                formData.append('ID', id);
                console.log(file);
                $.ajax({
                    url: url,
                    type: 'POST',
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        let result = JSON.parse(response);
                        console.log(result);
                        uploadfile.value = '';
                        window.location.replace(result.Link);
                    },
                    complete: function () {

                    },
                    error: function () {
                        alert('error alert');
                    },
                });
            };

            $("#upload360").hide();
            document.getElementById('finish').removeAttribute('disabled');
            document.getElementById('skip').removeAttribute('disabled');
            document.getElementById("finish").style.display = "none";
            document.getElementById("skip").style.display = "none";
        console.log(nextValue);


    });
}


function readySeats(){
    let continueTo=0;
    canvas.getObjects().forEach((obj) => {
        if (obj.positionId == '') {
            continueTo=1;
        }
    });

    if (continueTo==0){
        document.getElementById("upload360").style.display = "block";
        console.log(nextValue);

    }
    else {
        document.getElementById("upload360").style.display = "none";
        console.log(nextValue);

    }
}

let previous = $('#previous');
if(previous && previous.length > 0) {
    previous.click(function () {
        if (nextValue==1) {
            document.getElementById("upload360").style.display = "none";
            document.getElementById("skip").style.display = "none";
            document.getElementById("finish").style.display = "none";
            document.getElementById("previous").style.display = "none";
            document.getElementById("idSet").style.display = "block";
            document.getElementById("edit360").style.display = "none";
            document.getElementById("tools1").style.display = "block";
            document.getElementById("tools2").style.display = "block";
            document.getElementById("tools5").style.display = "block";
            document.getElementById("setSeatNo").style.display = "none";
            console.log(nextValue);

            try {
                canvas.discardActiveObject();
                let sel = new fabric.ActiveSelection(canvas.getObjects(), {
                    canvas: canvas,
                });
                canvas.setActiveObject(sel);
                canvas.requestRenderAll();

                if (canvas.getActiveObjects().length>0) {
                    canvas.getActiveObjects().forEach((obj) => {
                        obj.lockMovementX = false;
                        obj.lockMovementY = false;
                        obj.lockRotation = false;
                        obj.lockScalingX = false;
                        obj.lockScalingY = false;
                        obj.selectable = true;
                    });
                    canvas.renderAll();
                }

                canvas.discardActiveObject().renderAll()
            } catch (err) {
            }
        }
        if (nextValue==2) {
            document.getElementById('skip').removeAttribute('disabled');
            document.getElementById('upload360').removeAttribute('disabled');
            document.getElementById('previous').removeAttribute('disabled');
            document.getElementById("previous").style.display = "block";
            document.getElementById("skip").style.display = "block";
            document.getElementById("upload360").style.display = "none";
            document.getElementById("tools1").style.display = "none";
            document.getElementById("tools2").style.display = "none";
            document.getElementById("skip1").style.display = "none";
            document.getElementById("setSeatNo").style.display = "block";
            console.log(nextValue);


            link360 = div.attr('data-sphere');
            console.log(link360);
            if (link360!=0) {
                document.getElementById("edit360").style.display = "block";
                console.log(nextValue);

            }else{
                document.getElementById("edit360").style.display = "none";
                console.log(nextValue);

            }
            nextValue=1;
            console.log(nextValue);

        }
        if (nextValue==3) {
            $("#upload360").hide();
            document.getElementById('finish').removeAttribute('disabled');
            document.getElementById('skip').removeAttribute('disabled');
            document.getElementById("finish").style.display = "none";
            document.getElementById("upload360").style.display = "block";
            document.getElementById("skip").style.display = "none";
            document.getElementById("skip1").style.display = "block";
            document.getElementById("edit360").style.display = "none";
            console.log(nextValue);
            if (link360!=0) {
                document.getElementById("edit360").style.display = "block";
                console.log(nextValue);

            }else{
                document.getElementById("edit360").style.display = "none";
                console.log(nextValue);

            }
            nextValue=2;
        }

    });
}

let finish = $('#finish');
if(finish && finish.length > 0) {
    finish.click(function () {

        document.getElementById("upload360").style.display = "none";
        document.getElementById("previous").style.display = "none";
        document.getElementById("idSet").style.display = "none";
        document.getElementById("skip").style.display = "none";
        document.getElementById("finish").style.display = "none";
        console.log(nextValue);

        createFinalPlan();
    });
}

function createFinalPlanWithSphere(){

    const div = $('#sphere-editor');
    const link = div.attr('data-link');
    const planID = div.attr('data-id');
    const url = link + 'createfinalplanwithsphere';
    console.log(url);
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: {JSONCode: code, ID: planID},
        cache: true,
        success: function (response) {
            if(response.message == 'Success'){
                window.location.replace(response.Link);
            }else {
                return false;
            }

        },
        complete: function () {

        },
        error: function () {
            alert('error alert');
        },
    });
}

let finish360 = $('#finish360');
if(finish360 && finish360.length > 0) {
    finish360.click(function () {
        createFinalPlanWithSphere();
    });
}

function createFinalPlan() {
    const link = div.attr('data-link');
    const planID = div.attr('data-id');
    const url = link + 'createfinalplan';

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: {JSONCode: code, ID: planID},
        cache: true,
        success: function (response) {
            if(response.message == 'Success'){
                window.location.replace(response.Link);
            }else {
                return false;
            }

        },
        complete: function () {

        },
        error: function () {
            alert('error alert');
        },
    });

}

function save() {
    const link = div.attr('data-link');
    const planID = div.attr('data-id');
    const url = link + 'save';
    if (ready) {
console.log('testkkk');
        image = canvas.toSVG(['id','code','price','availability','positionId']);
        image = image.replace('width', '270');
        image = image.replace('height', '145');

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {JSONCode: JSON.stringify(canvas.toDatalessJSON(['id','code','price','availability','positionId'])), ID: planID , SVGImage: image},
            cache: true,
            success: function (response) {
                ready = false;

            },
            complete: function () {

            },
            error: function () {
                ready = false;
                alert('error alert');
            },
        });

    }

}
function getSVG() {
    const link = div.attr('data-link');
    const id = div.attr('data-id');
    const url = link + 'getcode';
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: {ID: id},
        cache: true,
        success: function (response) {
            canvas.loadFromDatalessJSON(JSON.parse(response.JSONCode));
        },
        complete: function () {

        },
        error: function () {
            alert('error alert');
        },
    });
}

if(div && div.length > 0 ) {
    window.setInterval(save, 2000);
    getSVG();
}
//trigger after every change in canvas
canvas.on('object:modified', function(options) {
    code = JSON.stringify(canvas.toDatalessJSON(['id','code','price','positionId','availability']));
    image = canvas.toSVG(['id','code','price','positionId','availability']);
    ready = true;
});


// Get the modal
let modal = document.getElementById("myModal");
let ShortKeymodal = document.getElementById("myShortKeyModal");

$(window).click (function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
});

$(window).click (function(event) {
    if (event.target == ShortKeymodal) {
        ShortKeymodal.style.display = "none";
    }
});

let seatNoArray=[];
let usedLetters=[];

let letterSelect =  $('#selectLetter');
if(letterSelect && letterSelect.length > 0) {
    letterSelect.click(function () {
        let positionNo=[];
        let selectedLetter = document.getElementById('dropList');
        let opt = selectedLetter.options[selectedLetter.selectedIndex];
        console.log( opt.value );
       let randomClr= getRandomColor();
        if (usedLetters.includes(opt.value)){
            alert("You have used this letter for a row, please select another letter!")
        }
        else {
            usedLetters.push(opt.value);
            seatNoArray = seatNoArray.filter(function( obj ) {
                return obj != opt.value;

            });
            console.log( seatNoArray );
            let selectedObjs = canvas.getActiveObjects();
            let Positioning=1;
            selectedObjs = selectedObjs.sort((a, b) => (a.left > b.left) ? 1 : -1);
            selectedObjs.forEach((obj) => {
                canvas.setActiveObject(obj);
                let activeObj = canvas.getActiveObject();
                if (obj.type == "rect") {
                    obj.set("positionId", opt.value+Positioning);
                }

                let textIn = new fabric.Text(opt.value+Positioning, {
                    fontFamily: 'Calibri',
                    fontSize: 10,
                    textAlign: 'center',
                    originX: -0.05,
                    originY: -0.1,
                    left: activeObj.left,
                    top: activeObj.top
                });
                // positionNo.push
                Positioning++;

                let ObjGroup = new fabric.Group([activeObj, textIn],{
                    // any group attributes here
                    left: activeObj.left,
                    top: activeObj.top
                });
                canvas.remove(activeObj);
                canvas.add(ObjGroup);

                ObjGroup.set('Code',ObjGroup.positionId)
                canvas.renderAll();
            });


            canvas.renderAll();
            console.log(selectedObjs);
            modal.style.display = "none";

            canvas.remove(selectedObjs);

            ready = true;
            save();
        }
        canvas.discardActiveObject().renderAll();
        readySeats();
    });
}

function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

let openModel = $('#setSeatNo');
if(openModel && openModel.length > 0) {
    openModel.click(function () {

        if (canvas.getActiveObjects().length>0) {
            modal.style.display = "block";

            for (let x = 1; x <= count; x++) {
                seatNoArray.push(toLetters(x));

            }
            console.log(seatNoArray);

            let select = document.getElementById("dropList");
            // var options = ["1", "2", "3", "4", "5"];

            for (let i = 0; i < seatNoArray.length; i++) {
                let opt = seatNoArray[i];
                let el = document.createElement("option");
                el.textContent = opt;
                el.value = opt;
                select.appendChild(el);
            }
        }
        else{
            alert("Please select a seat row to add Seat Numbers!")
        }
    });
}



let openBookingModel = $('#bookThisSeat');
if(openBookingModel && openBookingModel.length > 0) {
    openBookingModel.click(function () {


            modal.style.display = "block";



    });
}


let openShortKeyModel = $('#ShortKeys');
if(openShortKeyModel && openShortKeyModel.length > 0) {
    openShortKeyModel.click(function () {


        ShortKeymodal.style.display = "block";



    });
}

let closeModel =  $('#closeThis');
if(closeModel && closeModel.length > 0) {
    closeModel.click(function () {
        modal.style.display = "none";
    });
}

let closeShortKey =  $('#closeShortKey');
if(closeShortKey && closeShortKey.length > 0) {
    closeShortKey.click(function () {
        ShortKeymodal.style.display = "none";
    });
}


function toLetters(num) {
    "use strict";
    let mod = num % 26,
        pow = num / 26 | 0,
        out = mod ? String.fromCharCode(64 + mod) : (--pow, 'Z');
    return pow ? toLetters(pow) + out : out;
}




//save JS for canvas begin here========================================================================================
//separate>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// JavaScript Document
//save as png
let saveToPng=$("#save2Png");
if (saveToPng && saveToPng.length>0) {
    saveToPng.click(function () {

        window.open(canvas.toDataURL('image/png'));
        let gh = (canvas.toDataURL('png'));

        let a = document.createElement('a');
        a.href = gh;
        a.download = 'image.png';

        a.click();
        alert("converted");


    });
}






//save JS for canvas ends here========================================================================================
//separate>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


let signUp = $('#signUpButton');
if (signUp && signUp.length>0){
    signUp.click(function () {


        window.location.replace("/admin", "");

    });
}


let bookBtn = $('#bookSeatsBtn');
if (bookBtn && bookBtn.length>0){
    bookBtn.click(function () {


        window.location.replace("/event-manager", "");

    });
}

let colors = $('#clrBtn');
if (colors && colors.length>0) {
    colors.click(function () {


        let selectedColor = document.getElementById('colorpicked');
        console.log(selectedColor.value);


        let obj = canvas.getActiveObject();
        let objgroup = canvas.getActiveObjects();
        console.log(objgroup.length);
        if (obj) {
            obj.set('fill', selectedColor.value);
            canvas.renderAll();
        }
        if (!obj && !objgroup) {
            canvas.backgroundColor = selectedColor.value;
            canvas.renderAll();
        }
        try {
            if (objgroup.length > 1) {
                canvas.getActiveObjects().forEach((o) => {
                    o.set('fill', selectedColor.value);

                    canvas.renderAll();
                });
            }
        } catch (err) {
        }

    });

}

let _clipboard;
let copy = $('#copy');
if (copy && copy.length>0) {
    copy.click(function () {

        canvas.getActiveObject().clone(function(cloned) {
            _clipboard = cloned;

        });
    });

}



let paste = $('#paste');
if (paste && paste.length>0) {
    paste.click(function () {

        // clone again, so you can do multiple copies.
        _clipboard.clone(function(clonedObj) {
            canvas.discardActiveObject();
            clonedObj.set({
                left: clonedObj.left + 10,
                top: clonedObj.top + 10,
                evented: true,
            });
            if (clonedObj.type === 'activeSelection') {
                // active selection needs a reference to the canvas.
                clonedObj.canvas = canvas;
                clonedObj.forEachObject(function(obj) {
                    canvas.add(obj);
                });
                // this should solve the unselectability
                clonedObj.setCoords();
            } else {
                canvas.add(clonedObj);
            }
            _clipboard.top += 10;
            _clipboard.left += 10;
            canvas.setActiveObject(clonedObj);
            canvas.requestRenderAll();
        });
    });

}



let svgPlane = new String('<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="801px" height="334px" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 824 344" xmlns:xlink="http://www.w3.org/1999/xlink">  <g id="Layer_x0020_1">  <metadata id="CorelCorpID_0Corel-Layer"/>  <g>  <path fill="#E1E1E1" d="M725 45l52 0 -52 0zm52 0l-45 97 45 -97zm-79 83c48,17 52,26 56,36 41,1 63,3 70,8 -6,7 -30,9 -71,9 0,9 -6,18 -19,26l43 92 -51 1 -64 -73c-35,13 -93,21 -171,24l0 93 -163 0 -57 -94c-43,0 -86,-1 -130,-1 -7,0 -16,-3 -23,-5 -29,-8 -101,-28 -115,-58 -2,-4 -3,-8 -3,-13 0,-21 32,-49 110,-69 8,-1 17,-3 26,-5 7,-1 16,-1 24,-1 37,-1 75,-1 112,-2l54 -96 166 1 1 95c57,2 113,7 205,32zm-38 -9l65 -74 -65 74z"/>  <path id="1" fill="none" stroke="black" stroke-width="0.749302" d="M725 45l52 0m0 0l-45 97m-34 -14c48,17 52,26 56,36 41,1 63,3 70,8 -6,7 -30,9 -71,9 0,9 -6,18 -19,26l43 92 -51 1 -64 -73c-35,13 -93,21 -171,24l0 93 -163 0 -57 -94c-43,0 -86,-1 -130,-1 -7,0 -16,-3 -23,-5 -29,-8 -101,-28 -115,-58 -2,-4 -3,-8 -3,-13 0,-21 32,-49 110,-69 8,-1 17,-3 26,-5 7,-1 16,-1 24,-1 37,-1 75,-1 112,-2l54 -96 166 1 1 95c57,2 113,7 205,32zm-38 -9l65 -74"/>  </g>  <path fill="#E1E1E1" stroke="black" stroke-width="0.749302" d="M47 142c-10,9 -17,19 -16,32 0,5 4,14 6,18 3,4 6,8 9,12l1 0 16 0 0 0c-6,-6 -10,-15 -11,-30 -1,-12 3,-23 8,-32l-13 0z"/>  <path fill="none" stroke="black" stroke-width="0.749302" d="M754 164c-30,2 -42,5 -45,9 5,4 19,7 44,8"/>  <line fill="none" stroke="black" stroke-width="0.749302" x1="662" y1="227" x2="734" y2= "207" />  <path fill="#6B809B" stroke="#373435" stroke-width="0.749302" d="M698 128c16,6 26,10 34,14l45 -97 -52 0 -65 74c12,3 25,6 38,9z"/>  <path fill="#6B809B" stroke="#373435" stroke-width="0.749302" d="M824 172c-7,-5 -29,-7 -70,-8 -30,2 -42,5 -45,9 5,4 19,7 44,8 41,0 65,-2 71,-9z"/>  <polygon fill="#CCCCCC" stroke="#373435" stroke-width="0.749302" points="777,299 734,207 662,227 726,300 "/>  <path fill="#6B809B" stroke="#373435" stroke-width="0.749302" d="M753 181c41,0 65,-2 71,-9 -7,-5 -29,-7 -70,-8 -30,2 -42,5 -45,9 5,4 19,7 44,8z"/>  <path fill="#6B809B" stroke="#373435" stroke-width="0.749302" d="M732 142l45 -97 -52 0 -65 74c12,3 25,6 38,9 16,6 26,10 34,14z"/>  <path fill="#6B809B" stroke="#373435" stroke-width="0.749302" d="M732 142l45 -97 -52 0 -65 74c12,3 25,6 38,9 16,6 26,10 34,14z"/>  <path fill="#6B809B" stroke="#373435" stroke-width="0.749302" d="M732 142l45 -97 -52 0 -65 74c12,3 25,6 38,9 16,6 26,10 34,14z"/>  <path fill="#CCCCCC" stroke="#373435" stroke-width="0.749302" d="M732 142l45 -97 -52 0 -65 74c12,3 25,6 38,9 16,6 26,10 34,14z"/>  <path fill="#CCCCCC" stroke="#373435" stroke-width="0.749302" d="M824 172c-7,-5 -29,-7 -70,-8 -30,2 -42,5 -45,9 5,4 19,7 44,8 41,0 65,-2 71,-9z"/>  <path fill="none" stroke="#373435" stroke-width="0.749302" d="M52 174c-1,-12 3,-23 8,-32l-13 0c-10,9 -17,19 -16,32 0,5 4,14 6,18 3,4 6,8 9,12l1 0 16 0 0 0c-6,-6 -10,-15 -11,-30z"/>  <path fill="#6B809B" stroke="#373435" stroke-width="0.749302" d="M52 174c-1,-12 3,-23 8,-32l-13 0c-10,9 -17,19 -16,32 0,5 4,14 6,18 3,4 6,8 9,12l1 0 16 0 0 0c-6,-6 -10,-15 -11,-30z"/>  <path fill="none" stroke="#373435" stroke-width="0.749302" d="M52 174c-1,-12 3,-23 8,-32l-13 0c-10,9 -17,19 -16,32 0,5 4,14 6,18 3,4 6,8 9,12l1 0 16 0 0 0c-6,-6 -10,-15 -11,-30z"/>  <path fill="none" stroke="#373435" stroke-width="0.749302" d="M52 174c-1,-12 3,-23 8,-32l-13 0c-10,9 -17,19 -16,32 0,5 4,14 6,18 3,4 6,8 9,12l1 0 16 0 0 0c-6,-6 -10,-15 -11,-30z"/>  <path fill="#6B809B" stroke="#373435" stroke-width="0.749302" d="M52 174c-1,-12 3,-23 8,-32l-13 0c-10,9 -17,19 -16,32 0,5 4,14 6,18 3,4 6,8 9,12l1 0 16 0 0 0c-6,-6 -10,-15 -11,-30z"/>  <path fill="white" stroke="white" stroke-width="0.749302" d="M52 174c-1,-12 3,-23 8,-32l-13 0c-10,9 -17,19 -16,32 0,5 4,14 6,18 3,4 6,8 9,12l1 0 16 0 0 0c-6,-6 -10,-15 -11,-30z"/>  </g>  </svg>');


let svgAirplane = $('#plane');
if (svgAirplane && svgAirplane.length>0){
    svgAirplane.click(function () {

        fabric.loadSVGFromString(svgPlane, function(objects, options) {
            let obj = fabric.util.groupSVGElements(objects, options);
            canvas.add(obj).renderAll();
            obj.sendToBack();
            canvas.renderAll();
        });


    });

}

let svgBus = new String('<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="799px" height="202px" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 803 203" xmlns:xlink="http://www.w3.org/1999/xlink">  <g id="Layer_x0020_1"> <metadata id="CorelCorpID_0Corel-Layer"/> <line fill="none" stroke="black" stroke-width="0.780409" x1="793" y1="7" x2="16" y2= "5" /> <path fill="none" stroke="black" stroke-width="0.780409" d="M23 201c-17,-60 -19,-125 0,-201"/> <path fill="none" stroke="black" stroke-width="0.780409" d="M16 196l771 0c10,-62 12,-128 0,-196"/> <path fill="none" stroke="black" stroke-width="0.780409" d="M777 7c12,63 11,126 0,189l0 -2"/> <polygon fill="none" stroke="black" stroke-width="0.780409" points="25,72 111,73 124,83 123,119 113,128 25,129 "/> <path fill="none" stroke="black" stroke-width="0.780409" d="M36 5c-14,63 -14,127 -1,191"/> <polyline fill="none" stroke="black" stroke-width="0.780409" points="90,196 90,144 30,143 30,161 "/> <path fill="none" stroke="black" stroke-width="0.780409" d="M30 152l20 0 20 28c1,0 18,1 20,1l0 -18 -34 -1"/> <polyline fill="none" stroke="black" stroke-width="0.780409" points="26,72 27,71 40,72 41,14 34,13 "/> <polygon fill="none" stroke="black" stroke-width="0.780409" points="67,27 67,60 115,61 115,26 "/> <path fill="none" stroke="black" stroke-width="0.780409" d="M115 61l4 0c7,-10 8,-21 0,-35l-4 0"/> <path fill="#CCCCCC" stroke="#373435" stroke-width="0.780409" d="M25 129l1 -57 0 0c1,0 1,-1 1,-1 1,-19 3,-39 7,-58 1,-3 1,-5 2,-8l-14 0c-18,71 -16,134 0,191l13 0c-2,-12 -4,-23 -5,-35 -2,-10 -3,-21 -4,-32l-1 0z"/> <path fill="#6B809B" stroke="#373435" stroke-width="0.780409" d="M22 196c-16,-57 -18,-120 0,-191 0,-1 1,-3 1,-5l-7 0c-24,80 -18,144 0,201l7 0c0,-2 -1,-3 -1,-5z"/> <path fill="#6B809B" stroke="#373435" stroke-width="0.780409" d="M90 196l-55 0 -13 0c0,2 1,3 1,5l768 2c15,-55 17,-119 2,-196l-5 0c11,66 9,129 -1,189l-10 0 -687 0z"/> <path fill="#6B809B" stroke="#373435" stroke-width="0.780409" d="M36 5l104 0 637 2 11 0c0,-2 -1,-4 -1,-7l-764 0 -1 5c0,0 0,0 0,0l14 0z"/> <path fill="#6B809B" stroke="#373435" stroke-width="0.780409" d="M115 26l0 35 4 0c7,-10 8,-21 0,-35l-4 0z"/> <path fill="#6B809B" stroke="#373435" stroke-width="0.780409" d="M111 73l-71 -1 -14 0c-1,19 -1,38 0,57l87 -1 10 -9 1 -36 -13 -10z"/> <path fill="#6B809B" stroke="#373435" stroke-width="0.780409" d="M787 0c0,3 1,5 1,7l5 0c0,-2 -1,-4 -1,-7l-5 0z"/> <path fill="#CCCCCC" stroke="#373435" stroke-width="0.780409" d="M788 7l-11 0c12,63 11,126 0,189l10 0c10,-60 12,-123 1,-189z"/> <path fill="#CCCCCC" stroke="#373435" stroke-width="0.780409" d="M40 72l1 -58 -7 -1c-4,19 -6,39 -7,58l13 1z"/> <path fill="#E1E1E1" stroke="#373435" stroke-width="0.780409" d="M56 162l-9 -11 -19 1 2 9c1,12 3,23 5,35l55 0 0 -15c-5,0 -19,-1 -20,-1l-14 -18z"/> <path fill="#CCCCCC" stroke="#373435" stroke-width="0.780409" d="M90 163l-34 -1 14 18c1,0 15,1 20,1l0 -18z"/> <polygon fill="#CCCCCC" stroke="#373435" stroke-width="0.780409" points="56,162 90,163 90,144 30,143 30,152 47,151 "/> <polygon fill="#E1E1E1" stroke="#373435" stroke-width="0.780409" points="67,60 115,61 115,26 67,27 "/> <path fill="#999999" stroke="#373435" stroke-width="0.780409" d="M115 26l0 35 4 0c7,-10 8,-21 0,-35l-4 0z"/> <polygon fill="#999999" stroke="#373435" stroke-width="0.780409" points="90,144 28,143 28,152 48,153 56,162 90,163 "/> <polygon fill="#6B809B" stroke="#373435" stroke-width="0.780409" points="160,6 160,93 165,93 165,6 "/> <path fill="none" stroke="#373435" stroke-width="0.780409" d="M165 6l0 87 -5 0 0 -87 5 0zm-75 175l0 15 687 0c11,-63 12,-126 0,-189l-741 -2c-1,3 -1,5 -2,8l7 1 -1 58 71 1 13 10 -1 36 -10 9 -87 1c1,8 2,15 2,23l0 0 0 -9 62 1 0 19 0 18zm29 -155c8,14 7,25 0,35l-4 0 -48 -1 0 -33 48 -1 4 0z"/></g> </svg>');

let svgJumboBus = $('#bus');
if (svgJumboBus && svgJumboBus.length>0){
    svgJumboBus.click(function () {

        fabric.loadSVGFromString(svgBus, function(objects, options) {
            let obj = fabric.util.groupSVGElements(objects, options);
            canvas.add(obj).renderAll();
            obj.sendToBack();
            canvas.renderAll();
        });


    });

}


let svgDoor = new String('<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="50px" height="43px" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 104 90" xmlns:xlink="http://www.w3.org/1999/xlink">  <g id="Layer_x0020_1"> <metadata id="CorelCorpID_0Corel-Layer"/> <line fill="none" stroke="black" stroke-width="1.3797" x1="7" y1="78" x2="97" y2= "79" /> <polygon fill="none" stroke="black" stroke-width="1.3797" points="97,79 104,79 97,79 97,89 7,89 7,78 0,78 11,78 "/> <line fill="none" stroke="black" stroke-width="1.3797" x1="7" y1="89" x2="0" y2= "89" /> <line fill="none" stroke="black" stroke-width="1.3797" x1="97" y1="89" x2="103" y2= "89" /> <path fill="none" stroke="black" stroke-width="1.3797" d="M92 78c-1,-21 -8,-39 -21,-52 -16,-17 -35,-25 -58,-25l1 77 78 0z"/> </g> </svg>');

let svgDoorLeft = $('#door');
if (svgDoorLeft && svgDoorLeft.length>0){
    svgDoorLeft.click(function () {

        fabric.loadSVGFromString(svgDoor, function(objects, options) {
            let obj = fabric.util.groupSVGElements(objects, options);
            canvas.add(obj).renderAll();
            obj.sendToBack();
            canvas.renderAll();
        });


    });

}


let unselectable = $('#makeUnselectable');
if (unselectable && unselectable.length>0){
    unselectable.click(function () {

        if (canvas.getActiveObjects().length>0) {
            canvas.getActiveObjects().forEach((obj) => {

                obj.selectable = false;
            });
            canvas.renderAll();
        }
        canvas.discardActiveObject().renderAll()
    });

}

let selectable = $('#makeSelectable');
if (selectable && selectable.length>0){
    selectable.click(function () {
        canvas.discardActiveObject();
        let sel = new fabric.ActiveSelection(canvas.getObjects(), {
            canvas: canvas,
        });
        canvas.setActiveObject(sel);
        canvas.requestRenderAll();
        if (canvas.getActiveObjects().length>0) {
            canvas.getActiveObjects().forEach((obj) => {

                obj.selectable = true;
            });
            canvas.renderAll();
        }
        canvas.discardActiveObject().renderAll()
    });

}


canvas.on('mouse:wheel', function(opt) {
    let delta = opt.e.deltaY;
    let zoom = canvas.getZoom();
    zoom *= 0.999 ** delta;
    if (zoom > 20) zoom = 20;
    if (zoom < 0.01) zoom = 0.01;
    canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
    opt.e.preventDefault();
    opt.e.stopPropagation();
});

canvas.on('mouse:down', function(opt) {
    let evt = opt.e;
    if (evt.altKey === true) {
        this.isDragging = true;
        this.selection = false;
        this.lastPosX = evt.clientX;
        this.lastPosY = evt.clientY;
    }
});
canvas.on('mouse:move', function(opt) {
    if (this.isDragging) {
        let e = opt.e;
        let vpt = this.viewportTransform;
        vpt[4] += e.clientX - this.lastPosX;
        vpt[5] += e.clientY - this.lastPosY;
        this.requestRenderAll();
        this.lastPosX = e.clientX;
        this.lastPosY = e.clientY;
    }
});
canvas.on('mouse:up', function(opt) {
    this.isDragging = false;
    this.selection = true;
});


// define a handler
function oneItemDelete(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if ( e.keyCode == 46) {
        // call your function to do the thing
        try {
            let deleted=canvas.getActiveObjects();
            canvas.getActiveObjects().forEach((obj) => {
                canvas.remove(obj)
            });
            canvas.discardActiveObject().renderAll()
        }

        catch (err) {}
    }
}
// register the handler
document.addEventListener('keyup', oneItemDelete, false);

// define a handler
function doc_keyUp(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.keyCode == 46) {
        // call your function to do the thing
        if (confirm("Are You Sure??") == true) {
            canvas.clear();
            rectangle = [];
            id = [];

        } else {

        }
    }
}
// register the handler
document.addEventListener('keyup', doc_keyUp, false);

function keyCopy(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.keyCode == 67) {
        // call the function to do the thing
        canvas.getActiveObject().clone(function(cloned) {
            _clipboard = cloned;

        });
    }
}
// register the handler
document.addEventListener('keyup', keyCopy, false);



// define a handler
function keyPaste(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.keyCode == 86) {

        _clipboard.clone(function(clonedObj) {
            canvas.discardActiveObject();
            clonedObj.set({
                left: clonedObj.left + 10,
                top: clonedObj.top + 10,
                evented: true,
            });
            if (clonedObj.type === 'activeSelection') {
                // active selection needs a reference to the canvas.
                clonedObj.canvas = canvas;
                clonedObj.forEachObject(function(obj) {
                    canvas.add(obj);
                });
                // this should solve the unselectability
                clonedObj.setCoords();
            } else {
                canvas.add(clonedObj);
            }
            _clipboard.top += 10;
            _clipboard.left += 10;
            canvas.setActiveObject(clonedObj);
            canvas.requestRenderAll();
        });
    }
}
// register the handler
document.addEventListener('keyup', keyPaste, false);


// define a handler
function keyBold(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.keyCode == 66) {
        // call your function to do the thing
        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Text not selected");
        } else {
            if (obj.type == "i-text") {
                if (obj.fontStyle=="italic") {
                    obj.fontStyle="oblique";
                    canvas.renderAll();
                    console.log(obj);
                }
                else {
                    obj.fontStyle = "bold";
                    canvas.renderAll();
                }
            }
        }
    }
}
// register the handler
document.addEventListener('keyup', keyBold, false);


// define a handler
function keyItalic(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.keyCode == 73) {
        // call your function to do the thing
        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Text not selected");
        } else {
            if (obj.type == "i-text") {
                obj.fontStyle="italic";
                canvas.renderAll();
            }
        }
    }
}
// register the handler
document.addEventListener('keyup', keyItalic, false);

// define a handler
function keyUnderline(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.shiftKey && e.keyCode == 85) {
        // call your function to do the thing
        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Text not selected");
        } else {
            if (obj.type == "i-text") {
                obj.set('underline', true);
                console.log(obj.type);
                canvas.renderAll();
            }
        }
    }
}
// register the handler
document.addEventListener('keyup', keyUnderline, false);

// define a handler
function keyRegular(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.keyCode == 81) {
        // call your function to do the thing
        let obj = canvas.getActiveObject();
        if (!obj) {
            alert("Text not selected");
        } else {
            if (obj.type == "i-text") {
                obj.fontStyle="normal";
                obj.set('underline', false);
                canvas.renderAll();
            }
        }
    }
}
// register the handler
document.addEventListener('keyup', keyRegular, false);

// define a handler
function keySave(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.shiftKey && e.keyCode == 83) {
        // call your function to do the thing
        alert("saved");
        save();
    }
}
// register the handler
document.addEventListener('keyup', keySave, false);


function keyAddOneSeat(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.shiftKey && e.keyCode == 78) {
        // call your function to do the thing
        id.push(1);
        rectangle.push(new fabric.Rect({
            width: 25,
            height: 25,
            left: 60,
            top: 70,
            fill: '#e9967a',
            strokeWidth: 0.3,
            stroke: 'black',
            rx: 5,
            ry: 5,
            type: 'rect',
            availability: "available",
            positionId:"",
            code: "",
            price: '0.00',
            id:'0'
        }));

        let l = rectangle.length;
        canvas.add(rectangle[l - 1]);
    }
}
// register the handler
document.addEventListener('keyup', keyAddOneSeat, false);


// define a handler
function keyLock(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.shiftKey && e.keyCode == 76) {
        let objSelected=0;
// alert("test");
        try {

            if (canvas.getActiveObjects().length>0) {
                canvas.getActiveObjects().forEach((obj) => {
                    obj.lockMovementX = true;
                    obj.lockMovementY = true;
                    obj.lockRotation = true;
                    obj.lockScalingX = true;
                    obj.lockScalingY = true;
                    // obj.evented = false;
// console.log(obj);
                });
                canvas.renderAll();
                objSelected=canvas.getActiveObjects().length;

            }
            else if (objSelected==0) {

                alert("Select an object to lock");

            }

        } catch (err) {
        }
    }
}
// register the handler
document.addEventListener('keyup', keyLock, false);



function keyUnlock(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.shiftKey && e.keyCode == 75) {
        let objSelected=0;
// alert("test");
        try {

            if (canvas.getActiveObjects().length>0) {
                canvas.getActiveObjects().forEach((obj) => {
                    obj.lockMovementX = false;
                    obj.lockMovementY = false;
                    obj.lockRotation = false;
                    obj.lockScalingX = false;
                    obj.lockScalingY = false;
                    // obj.evented = false;
// console.log(obj);
                });
                canvas.renderAll();
                objSelected=canvas.getActiveObjects().length;

            }
            else if (objSelected==0) {

                alert("Select an object to unlock");

            }

        } catch (err) {
        }
    }
}
// register the handler
document.addEventListener('keyup', keyUnlock, false);


let sendEmail =  $('#SendMail');
if(sendEmail && sendEmail.length > 0) {
    sendEmail.click(function () {
        let address = document.getElementById("AddedEmail").value;
        // alert(address);
        Email.send({
            secureToken : "c25bb4b2-ced2-4099-a969-c3941530e099",
            To : "prabhashmj@gmail.com",
            From : "mapumj6@gmail.com",
            Subject : "Sending Emailasd  using javascript",
            Body : "Well that was easy!!",
        })
            .then(function (message) {
                alert("mail sent successfully")
            });
    });
}



        $('#canvasLink').each(function(){
            let newHref = $(this).attr("href").replace("?stage=Stage", "");
            $(this).attr("href", newHref);
        });


document.getElementById("Form_SearchForm_Description").setAttribute("placeholder", "Search");

let VendorCanvas = $('#CanvasPageLink');
if (VendorCanvas && VendorCanvas.length>0){
    VendorCanvas.click(function () {

     window.location.replace("/create-a-seat-plan", "");

    });
}

let VendorAdminLink = $('#VendorAdminLink');
if (VendorAdminLink && VendorAdminLink.length>0){
    VendorAdminLink.click(function () {

        window.location.replace("/admin", "");

    });
}

