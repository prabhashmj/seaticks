let button = $('#createNewPlan');
if(button && button.length > 0 ){
    const link = button.attr('data-link');
    const url = link + 'createEditablePlan';
    button.click(function () {
        let title = prompt("Please enter project name:", "");
        if (title == null || title == "") {
            return false;
        } else {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {Title: title},
                cache: true,
                success: function (response) {
                    if(response.canCreate){
                        window.location.replace(response.Link);
                    }else{
                        alert('you have reached maximun number of creations. Please upgrade your plan to unlock limitations.')
                    }


                },
                complete: function () {

                },
                error: function () {
                    alert('error alert');
                },
            });
        }
    });

}
